module OkcBot

  class Browser
    attr_reader :browser, :driver, :base_url

    def initialize(args)
      @username = args[:username]
      @password = args[:password]
      @driver    = args[:driver].to_sym
      @browser = Watir::Browser.new driver
      @base_url = "http://www.okcupid.com"
    end


    private

    def visit(page)
      begin
        @browser.goto "#{@base_url}/#{page}"
      rescue Errno::ECONNREFUSED
        puts "Webdriver reconnecting..."
        @browser  = Watir::Browser.new $driver.to_sym
        login
        @browser.goto "#{@base_url}/#{page}"
      end
    end

    def logged_in?
      # return @browser.h1(:id => 'home_heading').exists?
      return true
    end

    public

    def save_session
      @browser.cookies.to_a
    end

    def load_session(saved_cookies)
      @browser.goto("https://okcupid.com")
      @browser.cookies.clear
      saved_cookies.each do |saved_cookie|
        @browser.cookies.add(saved_cookie[:name], saved_cookie[:value])
      end
      @browser.goto("https://okcupid.com")
    end


    def load_more_messages
      @before = @browser.spans(:class => "previewline").to_a.size
      @browser.scroll.to :bottom
      sleep 1
      @after = @browser.spans(:class => "previewline").to_a.size
    end

    def no_new_messages_found
      @before == @after
    end

    def load_entire_page
      load_more_messages
      unless no_new_messages_found
        load_entire_page
      else
        @browser.scroll.to :top
      end
    end

    # randomized delays between actions minimizes bot detection
    def login
      @browser.goto("http://www.okcupid.com")
      @browser.link(:text => "Sign in").click
      sleep (1..3).to_a.sample.to_i
      @browser.text_field(:id => 'login_username').set(@username)
      sleep (1..3).to_a.sample.to_i
      @browser.text_field(:id => 'login_password').set(@password)
      sleep (1..3).to_a.sample.to_i
      @browser.button(:id => 'sign_in_button').click
      sleep (1..3).to_a.sample.to_i

      return logged_in?
    end


    def visit_messages_page
      visit "messages"
      load_entire_page

    end

    def visit_quickmatch_page
      visit "quickmatch"
    end

    def visit_im_history_page
      visit "imhistory"
    end

    def visit_sent_messages_page
      visit "messages?folder=2"
    end

    def visit_match_page
      visit "match"
    end

    def logout
      @browser.close
    end

  end

end
