module OkcBot

  class Main
    attr_reader :auth

    def initialize()
      @auth = Browser.new(username: @username, password: @password, driver: @driver)
      @auth.login
    end

    def exit
      @auth.logout
    end

  end
end
