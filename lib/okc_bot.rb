require_relative "okc_bot/version"
require 'watir-webdriver'
require 'watir-scroll'
require 'webdriver-user-agent'

module OkcBot

  Dir[File.dirname(__FILE__) + '/okc_bot/*.rb'].each do |file|
    require file
  end

end