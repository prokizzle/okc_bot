# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'okc_bot/version'

Gem::Specification.new do |spec|
  spec.name          = "okc_bot"
  spec.version       = OkcBot::VERSION
  spec.authors       = ["Nick Prokesch"]
  spec.email         = ["nick@prokes.ch"]
  spec.summary       = %q{A library for building okcupid bots}
  spec.description   = %q{Allows users to build bots that use okcupid}
  spec.homepage      = "http://nick.prokes.ch"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.require_paths    = ["lib"]

  spec.add_dependency "dotenv", "~> 2.0"
  spec.add_dependency "watir-webdriver", "~> 0.8"
  spec.add_dependency "webdriver-user-agent", "~> 7.3"
  spec.add_dependency "watir-scroll", "~> 0.1"
end
