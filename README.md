# OKCleanup
Clean up your OKCupid inbox!

Are you getting too many okcupid messages to deal with? Now you can easily bulk delete in batches.

## Requirements
- chromedriver
- phantomjs
- ruby (tested only in ruby 1.9.3-p194)

## Installation
1. Clone to your computer
`git clone http://github.com/prokizzle/okcleanup ~/okcleanup`
1. Install the headless browsers
`brew install chromedriver phantomjs`
1. Install required gems
`bundle install`

## Usage
- run `ruby bin/okcleanup`
- select one of the options

## Available cleanup options

### Match messages
This deletes all messages that say "It's a match!"

### Replied messages
Deletes all threads that you have replied to

### Missed IMs
Deletes all missed instant message notification from your messages inbox

### No photo
Deletes all messages from people without photos. This includes people who did not post a photo and people who have closed or disabled their accounts.

### Delete all
Deletes every message from the inbox (or tries to). It asks for confirmation first.

### Delete last page
Deletes only the final page of your messages, aka the oldest messages in your inbox.

## Known Issues
PhantomJS pretty much doesn't work right or consistently. I should probably spoof the user agent and try again.
